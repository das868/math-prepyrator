# math-prepyrator
When you attempt the Math A Levels, adequate preparation is key. If you want to start training for that and look for a suitable piece of software, your search ends right here! 

I do admit that it’s still sloppy and stub-leveled, but I plan to add more features apart from the core vector utils now in the future, such as a timer and further mental arithmetic.

Have fun with it, feel free to adapt it and do not give up on maths… It can be done, really ;-)
