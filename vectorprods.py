#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import random


class Vector:
    def __init__(self, *args):
        # Präzision: 3 Nachkommastellen
        self.values = [round(float(x), 3) for x in args]
        self.dimensions = len(args)

        if self.dimensions < 2:
            raise ValueError("Vector needs 2 or more arguments")
        elif self.dimensions == 2:
            self.x = self[0]
            self.y = self[1]
        else:
            for i, j in enumerate(self):
                setattr(self, "x"+str(i+1), j)

    def __repr__(self):
        return "Vector" + str(tuple(self))

    def __getitem__(self, key):
        if not isinstance(key, int):
            raise TypeError("Invalid key")
        else:
            res = self.values[key]
            if res == int(res):
                res = int(res)
            return res

    def __eq__(self, other):
        if isinstance(other, Vector):
            if self.dimensions == other.dimensions:
                return all([x == y for x, y in zip(self, other)])
            return False
        return NotImplemented

    def __abs__(self):
        return math.sqrt(sum([x * 2 for x in self]))

    def __neg__(self):
        return Vector(*[-x for x in self])

    def __add__(self, other):
        if isinstance(other, Vector) and self.dimensions == other.dimensions:
            new = [self[i] + other[i] for i in range(self.dimensions)]
            return Vector(*new)
        return NotImplemented

    def __sub__(self, other):
        if isinstance(other, Vector) and self.dimensions == other.dimensions:
            new = [self[i] - other[i] for i in range(self.dimensions)]
            return Vector(*new)
        return NotImplemented

    def __matmul__(self, other):
        # dot product
        if isinstance(other, Vector) and self.dimensions == other.dimensions:
            result = 0
            for s, o in zip(self, other):
                result += s * o
            return result
        return NotImplemented

    def __mul__(self, other):
        # multiplication with koefficient
        if isinstance(other, (int, float)):
            return Vector(*[other * x for x in self])
        # cross product
        elif isinstance(other, Vector):
            if self.dimensions == other.dimensions == 3:
                new = [
                    self[1] * other[2] - self[2] * other[1],
                    self[2] * other[0] - self[0] * other[2],
                    self[0] * other[1] - self[1] * other[0]
                ]
                return Vector(*new)
            elif self.dimensions == other.dimensions:
                raise NotImplementedError("You live in the wrong dimensions!")
        return NotImplemented

    def __rmul__(self, other):
        # multiplitcation with koefficient
        if isinstance(other, (int, float)):
            return Vector(*[other * x for x in self])
        return NotImplemented


def toVector(vec1, vec2, symbol):
    longest = max(
        max([len(str(x)) for x in vec1]), max([len(str(x)) for x in vec2]))
    v1 = [str(x).rjust(longest) for x in vec1]
    v2 = [str(x).rjust(longest) for x in vec2]
    print("\n/ " + v1[0] + " \\     / " + v2[0] + " \\" + "\n" + "| " + v1[1] +
          " |  " + symbol + "  | " + v2[1] + " |  =  ?" + "\n" + "\\ " + v1[2]
          + " /     \\ " + v2[2] + " /\n")


def haupt(zeichen, reps, grenze=9):
    score = 0.0
    for y in range(0, reps):
        vecA = Vector(*[random.randint(-grenze, grenze) for _ in range(3)])
        vecB = Vector(*[random.randint(-grenze, grenze) for _ in range(3)])
        toVector(vecA, vecB, zeichen)
        if zeichen == 'o':
            if int(input()) == vecA @ vecB:
                score += 1
                print("Right.")
            else:
                print("False, right answer:\t" + str(vecA @ vecB))
        elif zeichen == 'x':
            inlist = []
            inlist.append(int(input("x:\t")))
            inlist.append(int(input("y:\t")))
            inlist.append(int(input("z:\t")))
            if Vector(*inlist) == vecA * vecB:
                score += 1
                print("Right.")
            else:
                print("False, right answer:\t" + str(vecA * vecB))
        else:
            print("Internal Error:\tWrong character input.")
    print("\n------------------------------\nYour Score:\t" + str(int(score)) +
          "/" + str(reps))
    a = score / reps
    if a >= 0.96:
        print("Grade:\t15 P.")
    elif a >= 0.91:
        print("Grade:\t14 P.")
    elif a >= 0.86:
        print("Grade:\t13 P.")
    elif a >= 0.81:
        print("Grade:\t12 P.")
    elif a >= 0.76:
        print("Grade:\t11 P.")
    elif a >= 0.71:
        print("Grade:\t10 P.")
    elif a >= 0.66:
        print("Grade:\t9 P.")
    elif a >= 0.61:
        print("Grade:\t8 P.")
    elif a >= 0.56:
        print("Grade:\t7 P.")
    elif a >= 0.51:
        print("Grade:\t6 P.")
    elif a >= 0.46:
        print("Grade:\t5 P.")
    elif a >= 0.41:
        print("Grade:\t4 P.")
    elif a >= 0.36:
        print("Grade:\t3 P.")
    elif a >= 0.31:
        print("Grade:\t2 P.")
    elif a >= 0.26:
        print("Grade:\t1 P.")
    else:
        print("Grade:\t0 P.")


def main():
    rein = input("What do you wanna train?\n" +
                 "\t0\t–\tDot Product\n\t1\t–\tCross Product\n")
    reps = input("\nHow many Reps?\n")
    if rein == str(0):
        haupt('o', int(reps))
    elif rein == str(1):
        haupt('x', int(reps))
    else:
        print("Error!")


if __name__ == "__main__":
    main()
